import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile
import json
import argparse
    
from distutils.version import StrictVersion
from collections import defaultdict
from io import StringIO
from PIL import Image

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")
from object_detection.utils import ops as utils_ops

if StrictVersion(tf.__version__) < StrictVersion('1.12.0'):
    raise ImportError('Please upgrade your TensorFlow installation to v1.12.*.')


from utils import label_map_util
from utils import visualization_utils as vis_util


PATH_TO_FROZEN_GRAPH = '../models/inference_graph/frozen_inference_graph.pb'
PATH_TO_LABELS = '../data/label_map.pbtxt'
PATH_TO_OUTPUT = 'output'
PATH_TO_TEST_IMAGES_DIR = '../../dataset_builder/mask_dataset/images'


parser = argparse.ArgumentParser(description='Eval image directory')

parser.add_argument('--inference_graph', required=False,default=PATH_TO_FROZEN_GRAPH, help='Path to frozen_inference_graph.pb')
parser.add_argument('--labels', required=False,default=PATH_TO_LABELS, help='Path to label_map.pbtxt')
parser.add_argument('--output', required=False,default=PATH_TO_OUTPUT, help='Path to output dir')
parser.add_argument('--input', required=False,default=PATH_TO_TEST_IMAGES_DIR, help='Path to input dir')
parser.add_argument('--max',type=int, required=False,default=5, help='Number of images to process')

args = parser.parse_args()

PATH_TO_FROZEN_GRAPH = args.inference_graph
PATH_TO_LABELS = args.labels
PATH_TO_OUTPUT = args.output
PATH_TO_TEST_IMAGES_DIR = args.input

TEST_IMAGE_PATHS= [ os.path.join(PATH_TO_TEST_IMAGES_DIR , file) for file in os.listdir(PATH_TO_TEST_IMAGES_DIR) if file.endswith(('.png','.jpeg','.jpg'))]

if args.max > 0:
  TEST_IMAGE_PATHS = TEST_IMAGE_PATHS[:args.max]



detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_FROZEN_GRAPH, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')


category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS, use_display_name=True)

def load_image_into_numpy_array(image):
    (im_width, im_height) = image.size
    return np.array(image.getdata()).reshape((im_height, im_width, 3)).astype(np.uint8)

def run_inference_for_single_image(image, graph):
    with graph.as_default():
        with tf.Session() as sess:
            # Get handles to input and output tensors
            ops = tf.get_default_graph().get_operations()
            all_tensor_names = {output.name for op in ops for output in op.outputs}
            tensor_dict = {}
            for key in [
                'num_detections', 'detection_boxes', 'detection_scores',
                'detection_classes', 'detection_masks'
            ]:
                tensor_name = key + ':0'
                if tensor_name in all_tensor_names:
                    tensor_dict[key] = tf.get_default_graph().get_tensor_by_name(tensor_name)

                if 'detection_masks' in tensor_dict:
                    # The following processing is only for single image
                    detection_boxes = tf.squeeze(tensor_dict['detection_boxes'], [0])
                    detection_masks = tf.squeeze(tensor_dict['detection_masks'], [0])
                    # Reframe is required to translate mask from box coordinates to image coordinates and fit the image size.
                    real_num_detection = tf.cast(tensor_dict['num_detections'][0], tf.int32)
                    detection_boxes = tf.slice(detection_boxes, [0, 0], [real_num_detection, -1])
                    detection_masks = tf.slice(detection_masks, [0, 0, 0], [real_num_detection, -1, -1])
                    detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(detection_masks, detection_boxes, image.shape[1], image.shape[2])
                    detection_masks_reframed = tf.cast(tf.greater(detection_masks_reframed, 0.5), tf.uint8)
                    # Follow the convention by adding back the batch dimension
                    tensor_dict['detection_masks'] = tf.expand_dims(detection_masks_reframed, 0)
                image_tensor = tf.get_default_graph().get_tensor_by_name('image_tensor:0')

            # Run inference
            output_dict = sess.run(tensor_dict,feed_dict={image_tensor: image})

            # all outputs are float32 numpy arrays, so convert types as appropriate
            output_dict['num_detections'] = int(output_dict['num_detections'][0])
            output_dict['detection_classes'] = output_dict['detection_classes'][0].astype(np.int64)
            output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
            output_dict['detection_scores'] = output_dict['detection_scores'][0]
            if 'detection_masks' in output_dict:
                output_dict['detection_masks'] = output_dict['detection_masks'][0]
    return output_dict


full_dir = os.path.join(PATH_TO_OUTPUT, 'full')
mask_dir = os.path.join(PATH_TO_OUTPUT, 'mask')
boxes_dir = os.path.join(PATH_TO_OUTPUT, 'boxes')
original_dir = os.path.join(PATH_TO_OUTPUT, 'original')

if not os.path.exists(full_dir):
  os.makedirs(full_dir)

if not os.path.exists(mask_dir):
  os.makedirs(mask_dir)

if not os.path.exists(boxes_dir):
  os.makedirs(boxes_dir)

if not os.path.exists(original_dir):
  os.makedirs(original_dir)
    

for image_path in TEST_IMAGE_PATHS:
    
    image_name = os.path.splitext(os.path.basename(image_path))[0]
    print('Start processing ' + image_name)
    
    image = Image.open(image_path)
    # the array based representation of the image will be used later in order to prepare the
    # result image with boxes and labels on it.
    image_np = load_image_into_numpy_array(image)
    # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
    image_np_expanded = np.expand_dims(image_np, axis=0)
    # Actual detection.
    output_dict = run_inference_for_single_image(image_np_expanded, detection_graph)

    mask = output_dict.get('detection_masks')

    boxes = np.squeeze(output_dict['detection_boxes'])
    scores = np.squeeze(output_dict['detection_scores'])
    #set a min thresh score, say 0.8
    min_score_thresh = 0.8
    bboxes = boxes[scores > min_score_thresh]
    
    mask_np_black = image_np * 0

    #get image size
    im_width, im_height = image.size
    final_box = []
    i = 0
    
    json_data_list = []

    for box in bboxes:
        ymin, xmin, ymax, xmax = box
        final_box.append([xmin * im_width, xmax * im_width, ymin * im_height, ymax * im_height])
        
        name = category_index[output_dict['detection_classes'][i]]["name"]
        
        data = {}
        data[name] = [xmin * im_width, xmax * im_width, ymin * im_height, ymax * im_height]
        json_data_list.append(data);
        
        
        color = output_dict['detection_classes'][i] * 100;
                
        mask_np_black[:,:,0] = np.where(mask[i],color,mask_np_black[:,:,0])
        mask_np_black[:,:,1] = np.where(mask[i],color,mask_np_black[:,:,1])
        mask_np_black[:,:,2] = np.where(mask[i],color,mask_np_black[:,:,2])
        
        i = i + 1
    

        
    img = Image.fromarray(mask_np_black, 'RGB')
    img.save(os.path.join(mask_dir, image_name + ".jpeg"))
    
    
    print(json_data_list)
    
    with open(os.path.join(boxes_dir, image_name + ".json"), 'w') as f:
        f.write("%s\n" % json.dumps(json_data_list))
        

    img = Image.fromarray(image_np, 'RGB')
    img.save(os.path.join(original_dir ,image_name + ".jpeg"))


    # Visualization of the results of a detection.
    vis_util.visualize_boxes_and_labels_on_image_array(
    image_np,
    output_dict['detection_boxes'],
    output_dict['detection_classes'],
    output_dict['detection_scores'],
    category_index,
    instance_masks=mask,
    use_normalized_coordinates=True,
    line_thickness=8)

    img = Image.fromarray(image_np, 'RGB')
    img.save(os.path.join(full_dir ,image_name + ".jpeg"))

    print('Saved')