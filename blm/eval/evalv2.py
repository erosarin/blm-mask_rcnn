import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile
import json
	
from distutils.version import StrictVersion
from collections import defaultdict
from io import StringIO
from PIL import Image

from utils import label_map_util
from utils import visualization_utils as vis_util

from object_detection.utils import ops as utils_ops

# global variables 
category_index = 0
detection_graph = 0
final_box = []
sess = 0

picture_size = [720, 1280]

class ObjectDetection:

	def __init__():
		pass
		
	def open(self): 

		print("\nPY : start()")
		# This is needed since the notebook is stored in the object_detection folder.
		sys.path.append("..")
		
		if StrictVersion(tf.__version__) < StrictVersion('1.12.0'):
			raise ImportError('Please upgrade your TensorFlow installation to v1.12.*.')

		PATH_TO_FROZEN_GRAPH = '../models/inference_graph/frozen_inference_graph.pb'
		PATH_TO_LABELS = '../data/label_map.pbtxt'

		global category_index
		global detection_graph
		global sess
		global image_tensor
		global tensor_dict


		print("PY : detection_graph init")

		detection_graph = tf.Graph()
		with detection_graph.as_default():
			od_graph_def = tf.GraphDef()
			with tf.gfile.GFile(PATH_TO_FROZEN_GRAPH, 'rb') as fid:
				serialized_graph = fid.read()
				od_graph_def.ParseFromString(serialized_graph)
				tf.import_graph_def(od_graph_def, name='')

		sess = tf.Session(graph=detection_graph)

		print("PY : category_index init")

		category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS, use_display_name=True)

		print("PY : category_index init")

		ops = detection_graph.get_operations()
		all_tensor_names = {output.name for op in ops for output in op.outputs}
		tensor_dict = {}
		for key in [
				'num_detections', 'detection_boxes', 'detection_scores',
				'detection_classes', 'detection_masks'
		]:
			tensor_name = key + ':0'
			if tensor_name in all_tensor_names:
				tensor_dict[key] = detection_graph.get_tensor_by_name(tensor_name)

		if 'detection_masks' in tensor_dict:
			# The following processing is only for single image
			detection_boxes = tf.squeeze(tensor_dict['detection_boxes'], [0])
			detection_masks = tf.squeeze(tensor_dict['detection_masks'], [0])
			# Reframe is required to translate mask from box coordinates to image
			# coordinates and fit the image size.
			real_num_detection = tf.cast(tensor_dict['num_detections'][0], tf.int32)
			detection_boxes = tf.slice(detection_boxes, [0, 0], [real_num_detection, -1])
			detection_masks = tf.slice(detection_masks, [0, 0, 0],[real_num_detection, -1, -1])

			detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(detection_masks, detection_boxes, picture_size[0], picture_size[1])
			detection_masks_reframed = tf.cast(tf.greater(detection_masks_reframed, 0.5), tf.uint8)
			# Follow the convention by adding back the batch dimension
			tensor_dict['detection_masks'] = tf.expand_dims(detection_masks_reframed, 0)
		image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

		print("PY : started\n")


	def run_inference_for_single_image(image):
		
		global sess
		global detection_graph
		global tensor_dict

		print("PY : run_inference_for_single_image")
	   
		# Get handles to input and output tensors
		
		output_dict = sess.run(tensor_dict,feed_dict={image_tensor: image})

		# all outputs are float32 numpy arrays, so convert types as appropriate
		output_dict['num_detections'] = int(output_dict['num_detections'][0])
		output_dict['detection_classes'] = output_dict['detection_classes'][0].astype(np.int64)
		output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
		output_dict['detection_scores'] = output_dict['detection_scores'][0]
		if 'detection_masks' in output_dict:
			output_dict['detection_masks'] = output_dict['detection_masks'][0]

		print("PY : run_inference_for_single_image ok")

		return output_dict

	def get_bounding_boxes(self,image_np): 

		global category_index
		global final_box

		print("\nPY : getMask")

		#print(image_np.shape)
		#print(picture_size)

		# the array based representation of the image will be used later in order to prepare the
		# result image with boxes and labels on it.
		# Expand dimensions since the model expects images to have shape: [1, None, None, 3]
		image_np_expanded = np.expand_dims(image_np, axis=0)
		# Actual detection.

		output_dict = self.run_inference_for_single_image(image_np_expanded)

		print("PY : detection ok")

		mask = output_dict.get('detection_masks')

		boxes = np.squeeze(output_dict['detection_boxes'])
		scores = np.squeeze(output_dict['detection_scores'])
		#set a min thresh score, say 0.8
		min_score_thresh = 0.8
		bboxes = boxes[scores > min_score_thresh]
		
		mask_np_black = image_np * 0

		#get image size

		im_width, im_height, im_dim = image_np.shape
		final_box = []
		i = 0
		
		json_data_list = []

		for box in bboxes:
			ymin, xmin, ymax, xmax = box
			final_box.append([xmin * im_width, xmax * im_width, ymin * im_height, ymax * im_height])
			
			name = category_index[output_dict['detection_classes'][i]]["name"]
			
			data = {}
			data[name] = [xmin * im_width, xmax * im_width, ymin * im_height, ymax * im_height]
			json_data_list.append(data);
			
			
			color = output_dict['detection_classes'][i] * 100;
					
			mask_np_black[:,:,0] = np.where(mask[i],color,mask_np_black[:,:,0])
			mask_np_black[:,:,1] = np.where(mask[i],color,mask_np_black[:,:,1])
			mask_np_black[:,:,2] = np.where(mask[i],color,mask_np_black[:,:,2])
			
			i = i + 1
		

		#img = Image.fromarray(mask_np_black, 'RGB')
		#img.save(os.path.join(mask_dir, image_name + ".jpeg"))
		
		
		print("PY : " + str(json_data_list))
			   
		# Visualization of the results of a detection.
		vis_util.visualize_boxes_and_labels_on_image_array(
		image_np,
		output_dict['detection_boxes'],
		output_dict['detection_classes'],
		output_dict['detection_scores'],
		category_index,
		instance_masks=mask,
		use_normalized_coordinates=True,
		line_thickness=8)

		#img = Image.fromarray(image_np, 'RGB')
		#img.save(os.path.join(full_dir ,image_name + ".jpeg"))

		print('PY : getMask ok\n')

		return mask_np_black

	def getBoundingBoxes():
		global final_box
		return final_box