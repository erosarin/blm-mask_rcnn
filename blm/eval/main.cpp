
// python bindings
#include <Python.h>
#include <iostream>
#include <unistd.h>

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

#include <arrayobject.h>

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <fstream>
#include <string>
#include <iostream>
#include <assert.h>

using namespace std;
using namespace cv;

#define PATH_TO_FROZEN_GRAPH	"/home/erosarin/blm-mask_rcnn/blm/models/inference_graph/frozen_inference_graph.pb"
#define PATH_TO_LABELS 			"/home/erosarin/blm-mask_rcnn/blm/data/label_map.pbtxt"

// Image converter
PyObject* matToPython(Mat image);
Mat matFromPython(PyObject* result);



void processImage(PyObject* myClassInstance, String inputName, String outputName) {

	Mat image = imread(inputName, IMREAD_COLOR);
	assert(image.empty() == false);

	clock_t begin = clock();

	PyObject* imagep =  matToPython(image);
	assert(imagep != NULL);

	PyObject* resultGet = PyObject_CallMethod(myClassInstance, "get_bounding_boxes", "OO", myClassInstance, imagep);
	assert(resultGet != NULL);

	Mat mask = matFromPython(resultGet);
	assert(mask.empty() == false);

	clock_t end = clock();
  	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;

  	cout << elapsed_secs << " secs for processing " << inputName << " saving as " << outputName << endl;

	imwrite(outputName, mask);

	return;
}

int main(int argc, char** argv) {

	Py_Initialize();

	// this macro is defined be NumPy and must be included
	import_array1(-1);

	// setup file
	PyObject* module = PyImport_ImportModule("evalv2");
	assert(module != NULL);

	// setup class
	PyObject* MyPyClass = PyObject_GetAttrString(module, "ObjectDetection");
	assert(MyPyClass != NULL);

	// setup class instance
	PyObject* myClassInstance = PyInstanceMethod_New(MyPyClass);
	assert(myClassInstance != NULL);


	// call the init method in ObjectDetection class with self, and two strings as parameter, type object "O","s","s"
	//PyObject* resultInit = PyObject_CallMethod(myClassInstance, "init", "Oss", myClassInstance, PATH_TO_FROZEN_GRAPH, PATH_TO_LABELS);
	//assert(resultInit != NULL);

	// call the open method in ObjectDetection class with self as parameter, type object "O"
	PyObject* result2 = PyObject_CallMethod(myClassInstance, "open", "O", myClassInstance);
	assert(result2 != NULL);


	processImage(myClassInstance, "images/1.jpeg", "masks/mask1.jpg");
	processImage(myClassInstance, "images/2.jpeg", "masks/mask2.jpg");
	processImage(myClassInstance, "images/3.jpeg", "masks/mask3.jpg");


	Py_Finalize();

	return 0;
}



PyObject* matToPython(Mat image) {

	// total number of elements (here it's an RGB image)
	int nElem = image.rows * image.cols * 3;

	// create an array of apropriate datatype
	uchar* m = new uchar[nElem];

	// copy the data from the cv::Mat object into the array
	std::memcpy(m, image.data, nElem * sizeof(uchar));

	// the dimensions of the matrix
	npy_intp mdim[] = { image.rows, image.cols, 3 };

	// convert the cv::Mat to numpy.array
	return PyArray_SimpleNewFromData(3, mdim, NPY_UINT8, (void*) m);
}

Mat matFromPython(PyObject* result) {

	Mat out;

	if (!result) {
		cout << "No result" << endl;
		return out;
	}

	if (!PyArray_Check(result)) {
		cout << "Result is not an array" << endl;
		return out;
	}

	PyArrayObject *np_ret = reinterpret_cast<PyArrayObject*>(result);

	if (PyArray_NDIM(np_ret) == 2) {
		Mat mat2(PyArray_DIM(np_ret, 0), PyArray_DIM(np_ret, 1), CV_8UC1, PyArray_DATA(np_ret));
		out = mat2;
	}
	else if (PyArray_NDIM(np_ret) == 3) {
		Mat mat2(PyArray_DIM(np_ret, 0), PyArray_DIM(np_ret, 1), CV_8UC3, PyArray_DATA(np_ret));
		out = mat2;
	}

	return out;
}
