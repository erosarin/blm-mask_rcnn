"""


"""
from PIL import Image, ImageDraw

import os
import sys
import json
import numpy as np
import skimage.draw
import random
import tensorflow as tf
import argparse

def strObject(name,x,y):
	strXml   = "	<object>\n"
	strXml  += "		<name>" + name + "</name>\n"
	strXml  += "		<pose>Unspecified</pose>\n"
	strXml  += "		<truncated>0</truncated>\n"
	strXml  += "		<difficult>0</difficult>\n"
	strXml  += "		<bndbox>\n"
	strXml  += "			<xmin>" + str(min(x)) + "</xmin>\n"
	strXml  += "			<ymin>" + str(min(y)) + "</ymin>\n"
	strXml  += "			<xmax>" + str(max(x)) + "</xmax>\n"
	strXml  += "			<ymax>" + str(max(y)) + "</ymax>\n"
	strXml  += "		</bndbox>\n"
	strXml  += "	</object>\n"
	return strXml

mask_pixel = {'BLM_IC':100,'BLM_LIC':200}



parser = argparse.ArgumentParser(description='Create masks, xml and trainval for tensorflow.')

parser.add_argument('--input', required=True,metavar="via_dataset", help='Root directory to dataset folder that contains images.')
parser.add_argument('--output', required=True,metavar="mask_dataset", help='Root directory to dataset folder that contains images.')

args = parser.parse_args()

via_dir = args.input
dataset_dir = args.output


image_dir = os.path.join(dataset_dir, 'images')
mask_dir = os.path.join(dataset_dir, 'annotations', 'trimaps')
xml_dir = os.path.join(dataset_dir, 'annotations', 'xmls')
trainval_dir = os.path.join(dataset_dir, 'annotations')

annotations = json.load(open(os.path.join(via_dir , "via_region_data.json")))
annotations = list(annotations.values()) 
#annotations = [a for a in annotations if a['regions']]

if not os.path.exists(via_dir):
	exit()

if not os.path.exists(image_dir):
	os.makedirs(image_dir)

if not os.path.exists(mask_dir):
	os.makedirs(mask_dir)

if not os.path.exists(xml_dir):
	os.makedirs(xml_dir)

trainNames = []

imgCounter = 0;

# Add images
for a in annotations:

	imgCounter = imgCounter + 1

	bndboxCnt = 0

	polygons = [r['shape_attributes'] for r in a['regions']]
	names = [r['region_attributes'] for r in a['regions']]
	
	image_path = os.path.join(via_dir , a['filename'])
	image_base = skimage.io.imread(image_path)
	h, w = image_base.shape[:2]

	image_mask = Image.new('RGB', (w, h), 0)
	draw = ImageDraw.Draw(image_mask) 

	basename = "blm_" + str(imgCounter)# os.path.splitext(a['filename'])[0].replace(" ", "_");

	print("processing " + a['filename'] + " \t...",end = "");

	# Begin XML
	strXml   = "<annotation>\n"
	strXml  += "	<folder>images</folder>\n"
	strXml  += "	<filename>" + basename + ".jpeg" +"</filename>\n"
	strXml  += "	<path>"+ basename + ".jpeg" +"</path>\n"
	strXml  += "	<source>\n"
	strXml  += "		<database>Unknown</database>\n"
	strXml  += "	</source>\n"
	strXml  += "	<size>\n"
	strXml  += "		<width>" + str(w) + "</width>\n"
	strXml  += "		<height>" + str(h) + "</height>\n"
	strXml  += "		<depth>3</depth>\n"
	strXml  += "	</size>\n"
	strXml  += "	<segmented>0</segmented>\n"


	for idx,p in enumerate(polygons):
		x = p['all_points_x']
		y = p['all_points_y']
		idx = idx + 1

		bndboxCnt = bndboxCnt + 1

		vect = ()
		for i, xv in enumerate(x):
			vect += (xv , y[i]),
		
		className = names[idx - 1]['name']
		tint = mask_pixel[className]
		color = (tint,tint,tint)

		strXml  += strObject(className,x,y)

		draw.polygon(vect, fill=color, outline=None)

	# End XML
	strXml  += "</annotation>\n"

	trainNames.append(basename)
	#print(basename)

	skimage.io.imsave(os.path.join(image_dir , basename + ".jpeg" ),image_base)
	image_mask.save(os.path.join(mask_dir , basename + ".png" ))

	with open(os.path.join(xml_dir , basename + ".xml"), 'w') as f:
		f.write("%s" % strXml)

	print("OK " + str(bndboxCnt) + " object(s)");


random.shuffle(trainNames)

with open(os.path.join(trainval_dir , "trainval.txt"), 'w') as f:
	for name in trainNames:

		f.write("%s\n" % name)

print(str(imgCounter) + " images processed");
